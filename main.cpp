#include <iostream>

using namespace std;

void a()
{
    cout << "      " <<endl;
    cout << "      " <<endl;
    cout << "   /\\   " <<endl;
    cout << "  /--\\ " <<endl;
    cout << " /    \\ " <<endl;
    cout << "      " <<endl;
}

void l()
{
    cout << "      " << endl;
    cout << " ||   " << endl;
    cout << " ||   " << endl;
    cout << " ||   " << endl;
    cout << " ||   " << endl;
    cout << " ||____ " << endl;
}

void e()
{
    cout << "      " << endl;
    cout << " |---- " << endl;
    cout << " |    " << endl;
    cout << " |---- " << endl;
    cout << " |    " << endl;
    cout << " |---- " << endl;
}


void x()
{
    cout << "      " << endl;
    cout << " \\   /" << endl;
    cout << "  \\ / " << endl;
    cout << "  /\\ " << endl;
    cout << "      " << endl;
    cout << "      " << endl;
}

void i()
{
    cout << "   00  " << endl;
    cout << "       " << endl;
    cout << "   ||   " << endl;
    cout << "   ||   " << endl;
    cout << "   ||   " << endl;
    cout << "   ||   " << endl;
}

int main()
{
    a(); l(); e(); x(); e(); i();
    return 0;
}
